# DTPD-Data collection
Note: To include the guidelines in the reporting template, the reportingtemplate_guidelines.xlsx must be stored in the folder pointed to by the "Output folder" parameter.

The data collection concerns the collection of financial disclosures from the Reporting Entity to the Reporting Host. The disclosure, to be entered into an Excel reporting template structured according to the “information matrix”, submitted to a secure file directory ‘hot folder’ organised by Reporting Entity who has been granted access to it.  
The processing and transformation of the received information is to be manually triggered by the Reporting Host (data analyst) with an automated transformation to a standardised “SDMX” input that is pushed to the data collection environment, to store, validate, and view individual records.  Notifications to the Data Provider initiated on certain predefined actions e.g. when a report is submitted for processing.  
  
The Data Collection is based on the DTPD reporting SDMX structure. The DTPD reporting SDMX structure can be changed by the reporting host according to the following workflow:   
   
![Structure change workflow](./statics/dtpd-diagram-of-the-flow-on-structure-changes.png)   
  
